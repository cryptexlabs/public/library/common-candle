"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var CandleSchemaVersions;
(function (CandleSchemaVersions) {
    CandleSchemaVersions["CURRENT_VERSION"] = "1.0.0";
    CandleSchemaVersions["V1_0_0"] = "1.0.0";
})(CandleSchemaVersions = exports.CandleSchemaVersions || (exports.CandleSchemaVersions = {}));
//# sourceMappingURL=candle.schema-versions.js.map