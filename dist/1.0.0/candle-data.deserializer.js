"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const candle_deserializer_1 = require("./candle.deserializer");
const common_trade_1 = require("@cryptex-labs/common-trade");
class CandleDataDeserializer {
    constructor(candleData) {
        this.candleData = candleData;
        this.candle = new candle_deserializer_1.CandleDeserializer(candleData.candle);
        this.lastTrades = [];
        for (const trade of candleData.lastTrades) {
            this.lastTrades.push(new common_trade_1.TradeDeserializer(trade));
        }
    }
    getGranularity() {
        return this.candleData.granularity;
    }
    getLastTrades() {
        return this.lastTrades;
    }
    getCandle() {
        return this.candle;
    }
}
exports.CandleDataDeserializer = CandleDataDeserializer;
//# sourceMappingURL=candle-data.deserializer.js.map