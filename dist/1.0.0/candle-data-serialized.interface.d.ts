import { CandleSerializedInterface } from "./candle-serialized.interface";
import { TradeSerializedInterface } from "@cryptex-labs/common-trade";
export interface CandleDataSerializedInterface {
    granularity: number;
    candle: CandleSerializedInterface;
    lastTrades: TradeSerializedInterface[];
}
