import { CandleDataInterface } from "../candle-data.interface";
export declare class CandleStreamRecordDeserializer {
    private candleDatum;
    constructor(data: any[]);
    getCandleDatum(): CandleDataInterface[];
}
