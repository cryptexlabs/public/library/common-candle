"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const candle_datum_deserializer_1 = require("../candle-datum.deserializer");
const candle_schema_1 = require("../candle-schema");
class CandleStreamRecordDeserializer {
    constructor(data) {
        this.candleDatum = [];
        if (typeof data[0] === "undefined") {
            return;
        }
        if (typeof data[1] === "undefined") {
            return;
        }
        const rawRecordData = data[1];
        if (typeof rawRecordData[0] === "undefined") {
            return;
        }
        if (typeof rawRecordData[1] === "undefined") {
            return;
        }
        if (rawRecordData[0] !== candle_schema_1.CandleSchema.getCandleUpdateStreamKey()) {
            return;
        }
        const jsonCandleData = rawRecordData[1];
        if (jsonCandleData === "") {
            return;
        }
        const rawCandleDatum = JSON.parse(jsonCandleData);
        const deserializer = new candle_datum_deserializer_1.CandleDatumDeserializer(rawCandleDatum);
        this.candleDatum = deserializer.getCandleDatum();
    }
    getCandleDatum() {
        return this.candleDatum;
    }
}
exports.CandleStreamRecordDeserializer = CandleStreamRecordDeserializer;
//# sourceMappingURL=candle-stream-record.deserializer.js.map