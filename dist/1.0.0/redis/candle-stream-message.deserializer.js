"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const candle_stream_record_deserializer_1 = require("./candle-stream-record.deserializer");
class CandleStreamMessageDeserializer {
    constructor(message) {
        this.candles = [];
        if (!message) {
            return;
        }
        if (!Array.isArray(message)) {
            return;
        }
        if (typeof message[0] === "undefined") {
            return;
        }
        if (typeof message[0][1] === "undefined") {
            return;
        }
        if (typeof message[0][1][0] === "undefined") {
            return;
        }
        const rawCandleStream = message[0][1];
        for (const rawRecord of rawCandleStream) {
            const record = new candle_stream_record_deserializer_1.CandleStreamRecordDeserializer(rawRecord);
            const candleDatum = record.getCandleDatum();
            for (const candleData of candleDatum) {
                this.candles.push(candleData);
            }
        }
    }
    getCandles() {
        return this.candles;
    }
}
exports.CandleStreamMessageDeserializer = CandleStreamMessageDeserializer;
//# sourceMappingURL=candle-stream-message.deserializer.js.map