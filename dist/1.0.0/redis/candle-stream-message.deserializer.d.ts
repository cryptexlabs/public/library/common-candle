import { CandleDataInterface } from "../candle-data.interface";
export declare class CandleStreamMessageDeserializer {
    private candles;
    constructor(message: any);
    getCandles(): CandleDataInterface[];
}
