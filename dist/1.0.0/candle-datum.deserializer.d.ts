import { CandleDataInterface } from "./candle-data.interface";
import { CandleDataSerializedInterface } from "./candle-data-serialized.interface";
export declare class CandleDatumDeserializer {
    private readonly rawCandleDatum;
    private candleDatum;
    constructor(rawCandleDatum: CandleDataSerializedInterface[]);
    getCandleDatum(): CandleDataInterface[];
}
