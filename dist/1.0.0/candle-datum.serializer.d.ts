import { CandleDataInterface } from "./candle-data.interface";
import { CandleDataSerializedInterface } from "./candle-data-serialized.interface";
export declare class CandleDatumSerializer {
    private serializedCandleDatum;
    constructor(candleDatum: CandleDataInterface[]);
    getDatum(): CandleDataSerializedInterface[];
}
