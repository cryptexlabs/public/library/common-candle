"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class CandleUtil {
    constructor() {
    }
    static getGranularitiesFromCandleDatum(candleDatum) {
        const granularities = [];
        for (const candleData of candleDatum) {
            granularities.push(candleData.getGranularity());
        }
        return granularities;
    }
}
exports.CandleUtil = CandleUtil;
//# sourceMappingURL=candle-util.js.map