export interface CandleInterface {
    getVolume(): string;
    getOpen(): string;
    getHigh(): string;
    getLow(): string;
    getClose(): string;
    getTime(): number;
}
