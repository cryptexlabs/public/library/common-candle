import { CandleDataInterface } from "./candle-data.interface";
import { CandleInterface } from "./candle.interface";
import { TradeInterface } from "@cryptex-labs/common-trade";
import { CandleDataSerializedInterface } from "./candle-data-serialized.interface";
export declare class CandleDataDeserializer implements CandleDataInterface {
    private readonly candleData;
    private readonly candle;
    private readonly lastTrades;
    constructor(candleData: CandleDataSerializedInterface);
    getGranularity(): number;
    getLastTrades(): TradeInterface[];
    getCandle(): CandleInterface;
}
