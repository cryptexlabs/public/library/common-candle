"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const candle_serializer_1 = require("./candle.serializer");
const common_trade_1 = require("@cryptex-labs/common-trade");
class CandleDataSerializer {
    constructor(candleData) {
        this.candle = new candle_serializer_1.CandleSerializer(candleData.getCandle());
        this.granularity = candleData.getGranularity();
        const tradesSerializer = new common_trade_1.TradeDatumSerializer(candleData.getLastTrades());
        this.lastTrades = tradesSerializer.getDatum();
    }
}
exports.CandleDataSerializer = CandleDataSerializer;
//# sourceMappingURL=candle-data.serializer.js.map