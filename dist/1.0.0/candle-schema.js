"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const path = require("path");
class CandleSchema {
    static getVersion() {
        return this.version;
    }
    static getCandleUpdateMessageId() {
        return "candle";
    }
    static getCandleUpdateStreamKey() {
        return this.getCandleUpdateMessageId() + "-" + this.getVersion();
    }
}
CandleSchema.version = path.basename(__dirname);
exports.CandleSchema = CandleSchema;
//# sourceMappingURL=candle-schema.js.map