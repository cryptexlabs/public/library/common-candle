"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const candle_data_deserializer_1 = require("./candle-data.deserializer");
class CandleDatumDeserializer {
    constructor(rawCandleDatum) {
        this.rawCandleDatum = rawCandleDatum;
        this.candleDatum = [];
        for (const rawCandleData of rawCandleDatum) {
            this.candleDatum.push(new candle_data_deserializer_1.CandleDataDeserializer(rawCandleData));
        }
    }
    getCandleDatum() {
        return this.candleDatum;
    }
}
exports.CandleDatumDeserializer = CandleDatumDeserializer;
//# sourceMappingURL=candle-datum.deserializer.js.map