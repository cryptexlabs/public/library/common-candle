"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const candle_data_serializer_1 = require("./candle-data.serializer");
class CandleDatumSerializer {
    constructor(candleDatum) {
        this.serializedCandleDatum = [];
        for (const candleData of candleDatum) {
            this.serializedCandleDatum.push(new candle_data_serializer_1.CandleDataSerializer(candleData));
        }
    }
    getDatum() {
        return this.serializedCandleDatum;
    }
}
exports.CandleDatumSerializer = CandleDatumSerializer;
//# sourceMappingURL=candle-datum.serializer.js.map