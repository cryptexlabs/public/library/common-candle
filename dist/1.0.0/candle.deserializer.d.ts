import { CandleSerializer } from "./candle.serializer";
import { CandleInterface } from "./candle.interface";
export declare class CandleDeserializer implements CandleInterface {
    private readonly candle;
    constructor(candle: CandleSerializer);
    getVolume(): string;
    getOpen(): string;
    getHigh(): string;
    getLow(): string;
    getClose(): string;
    getTime(): number;
}
