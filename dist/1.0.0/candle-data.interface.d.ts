import { CandleInterface } from "./candle.interface";
import { TradeInterface } from "@cryptex-labs/common-trade";
export interface CandleDataInterface {
    getGranularity(): number;
    getLastTrades(): TradeInterface[];
    getCandle(): CandleInterface;
}
