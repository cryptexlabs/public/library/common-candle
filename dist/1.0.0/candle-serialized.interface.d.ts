export interface CandleSerializedInterface {
    open: string;
    high: string;
    low: string;
    close: string;
    volume: string;
    time: number;
}
