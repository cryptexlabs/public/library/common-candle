import { CandleInterface } from "./candle.interface";
import { CandleSerializedInterface } from "./candle-serialized.interface";
export declare class CandleSerializer implements CandleSerializedInterface {
    readonly open: string;
    readonly high: string;
    readonly low: string;
    readonly close: string;
    readonly volume: string;
    readonly time: number;
    constructor(candle: CandleInterface);
}
