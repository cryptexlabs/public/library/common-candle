"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class CandleDeserializer {
    constructor(candle) {
        this.candle = candle;
    }
    getVolume() {
        return this.candle.volume;
    }
    getOpen() {
        return this.candle.open;
    }
    getHigh() {
        return this.candle.high;
    }
    getLow() {
        return this.candle.low;
    }
    getClose() {
        return this.candle.close;
    }
    getTime() {
        return this.candle.time;
    }
}
exports.CandleDeserializer = CandleDeserializer;
//# sourceMappingURL=candle.deserializer.js.map