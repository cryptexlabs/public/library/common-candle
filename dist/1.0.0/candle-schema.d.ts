export declare class CandleSchema {
    private static readonly version;
    static getVersion(): string;
    static getCandleUpdateMessageId(): string;
    static getCandleUpdateStreamKey(): string;
}
