import { CandleDataInterface } from "./candle-data.interface";
export declare class CandleUtil {
    private constructor();
    static getGranularitiesFromCandleDatum(candleDatum: CandleDataInterface[]): number[];
}
