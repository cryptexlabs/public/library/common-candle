"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class CandleSerializer {
    constructor(candle) {
        this.open = candle.getOpen();
        this.high = candle.getHigh();
        this.low = candle.getLow();
        this.close = candle.getClose();
        this.volume = candle.getVolume();
        this.time = candle.getTime();
    }
}
exports.CandleSerializer = CandleSerializer;
//# sourceMappingURL=candle.serializer.js.map