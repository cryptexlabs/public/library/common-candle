import { CandleDataInterface } from "./candle-data.interface";
import { CandleSerializer } from "./candle.serializer";
import { TradeSerializedInterface } from "@cryptex-labs/common-trade";
export declare class CandleDataSerializer {
    readonly granularity: number;
    readonly candle: CandleSerializer;
    readonly lastTrades: TradeSerializedInterface[];
    constructor(candleData: CandleDataInterface);
}
