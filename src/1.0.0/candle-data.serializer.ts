import {CandleDataInterface} from "./candle-data.interface";
import {CandleSerializer} from "./candle.serializer";
import {TradeDatumSerializer, TradeSerializedInterface} from "@cryptex-labs/common-trade";

export class CandleDataSerializer {

    public readonly granularity: number;
    public readonly candle: CandleSerializer;
    public readonly lastTrades: TradeSerializedInterface[];

    constructor(candleData: CandleDataInterface) {
        this.candle      = new CandleSerializer(candleData.getCandle());
        this.granularity = candleData.getGranularity();

        const tradesSerializer = new TradeDatumSerializer(candleData.getLastTrades());
        this.lastTrades        = tradesSerializer.getDatum();
    }

}