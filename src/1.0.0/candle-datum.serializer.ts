import {CandleDataInterface} from "./candle-data.interface";
import {CandleDataSerializedInterface} from "./candle-data-serialized.interface";
import {CandleDataSerializer} from "./candle-data.serializer";

export class CandleDatumSerializer {

    private serializedCandleDatum: CandleDataSerializedInterface[];

    constructor(candleDatum: CandleDataInterface[]) {
        this.serializedCandleDatum = [];
        for (const candleData of candleDatum) {
            this.serializedCandleDatum.push(new CandleDataSerializer(candleData));
        }
    }

    public getDatum(): CandleDataSerializedInterface[] {
        return this.serializedCandleDatum;
    }
}