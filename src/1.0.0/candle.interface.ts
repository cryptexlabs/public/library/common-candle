export interface CandleInterface {

    /**
     * The total quantity of the asset that was traded for the bucket
     *
     * @return {string}
     */
    getVolume(): string;

    /**
     *
     *
     * @return {string}
     */
    getOpen(): string;

    /**
     * The highest sale price that fell inside the bucket
     *
     * @return {string}
     */
    getHigh(): string;

    /**
     * The lowest trade price that fell inside the bucket
     *
     * @return {string}
     */
    getLow(): string;

    /**
     * The last trade price that fell inside the bucket
     *
     * @return {string}
     */
    getClose(): string;

    /**
     * The startCandleStreamService time of the bucket
     *
     * @return {number}
     */
    getTime(): number;

}