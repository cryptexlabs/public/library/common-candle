import {CandleInterface} from "./candle.interface";
import {TradeInterface} from "@cryptex-labs/common-trade";

export interface CandleDataInterface {

    /**
     * Size of the bucket in seconds
     *
     * @return {number}
     */
    getGranularity(): number;

    /**
     * Gets the trade ids that were responsible for the last change to the candle for the time of the candle.
     *
     * @return {string}
     */
    getLastTrades(): TradeInterface[];

    /**
     * The candle
     * @return {CandleInterface}
     */
    getCandle(): CandleInterface;
}