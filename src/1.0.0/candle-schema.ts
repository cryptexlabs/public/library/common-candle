import path = require("path");

export class CandleSchema {

  private static readonly version = path.basename(__dirname);

  public static getVersion(): string {
    return this.version;
  }

  public static getCandleUpdateMessageId(): string {
    return "candle";
  }

  public static getCandleUpdateStreamKey() {
    return this.getCandleUpdateMessageId() + "-" + this.getVersion();
  }
}