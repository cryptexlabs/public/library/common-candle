import {CandleSerializer} from "./candle.serializer";
import {CandleInterface} from "./candle.interface";

export class CandleDeserializer implements CandleInterface {

    constructor(private readonly candle: CandleSerializer) {
    }

    public getVolume(): string {
        return this.candle.volume;
    }

    public getOpen(): string {
        return this.candle.open;
    }

    public getHigh(): string {
        return this.candle.high;
    }

    public getLow(): string {
        return this.candle.low;
    }

    public getClose(): string {
        return this.candle.close;
    }

    public getTime(): number {
        return this.candle.time;
    }
}