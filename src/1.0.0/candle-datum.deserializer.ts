import {CandleDataInterface} from "./candle-data.interface";
import {CandleDataDeserializer} from "./candle-data.deserializer";
import { CandleDataSerializedInterface } from "./candle-data-serialized.interface";

export class CandleDatumDeserializer {

    private candleDatum: CandleDataInterface[];

    constructor(private readonly rawCandleDatum: CandleDataSerializedInterface[]) {
        this.candleDatum = [];
        for (const rawCandleData of rawCandleDatum) {
            this.candleDatum.push(new CandleDataDeserializer(rawCandleData));
        }
    }

    public getCandleDatum(): CandleDataInterface[] {
        return this.candleDatum;
    }
}