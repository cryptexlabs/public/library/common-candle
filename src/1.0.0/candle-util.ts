import { CandleDataInterface } from "./candle-data.interface";

export class CandleUtil {

  private constructor() {
  }

  public static getGranularitiesFromCandleDatum(candleDatum: CandleDataInterface[]): number[] {
    const granularities = [];
    for (const candleData of candleDatum) {
      granularities.push(candleData.getGranularity());
    }
    return granularities;
  }
}