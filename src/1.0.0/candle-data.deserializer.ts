import {CandleDataInterface} from "./candle-data.interface";
import {CandleInterface} from "./candle.interface";
import {CandleDeserializer} from "./candle.deserializer";
import {TradeInterface, TradeDeserializer} from "@cryptex-labs/common-trade";
import {CandleDataSerializedInterface} from "./candle-data-serialized.interface";

export class CandleDataDeserializer implements CandleDataInterface {

    private readonly candle: CandleInterface;
    private readonly lastTrades: TradeInterface[];

    constructor(private readonly candleData: CandleDataSerializedInterface) {
        this.candle     = new CandleDeserializer(candleData.candle);
        this.lastTrades = [];
        for (const trade of candleData.lastTrades) {
            this.lastTrades.push(new TradeDeserializer(trade));
        }
    }

    public getGranularity(): number {
        return this.candleData.granularity;
    }

    public getLastTrades(): TradeInterface[] {
        return this.lastTrades;
    }

    public getCandle(): CandleInterface {
        return this.candle;
    }

}