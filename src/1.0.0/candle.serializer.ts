import {CandleInterface} from "./candle.interface";
import {CandleSerializedInterface} from "./candle-serialized.interface";

export class CandleSerializer implements CandleSerializedInterface {

    public readonly open: string;
    public readonly high: string;
    public readonly low: string;
    public readonly close: string;
    public readonly volume: string;
    public readonly time: number;

    constructor(candle: CandleInterface) {
        this.open   = candle.getOpen();
        this.high   = candle.getHigh();
        this.low    = candle.getLow();
        this.close  = candle.getClose();
        this.volume = candle.getVolume();
        this.time   = candle.getTime();
    }
}