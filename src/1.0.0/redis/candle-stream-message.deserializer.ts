import {CandleDataInterface} from "../candle-data.interface";
import {CandleStreamRecordDeserializer} from "./candle-stream-record.deserializer";

export class CandleStreamMessageDeserializer {

    private candles: CandleDataInterface[];

    constructor(message: any) {
        this.candles = [];
        if (!message) {
            return;
        }
        if (!Array.isArray(message)) {
            return;
        }
        if (typeof message[0] === "undefined") {
            return;
        }
        if (typeof message[0][1] === "undefined") {
            return;
        }
        if (typeof message[0][1][0] === "undefined") {
            return;
        }

        const rawCandleStream = message[0][1];

        for (const rawRecord of rawCandleStream) {
            const record      = new CandleStreamRecordDeserializer(rawRecord);
            const candleDatum = record.getCandleDatum();
            for (const candleData of candleDatum) {
                this.candles.push(candleData);
            }
        }
    }

    public getCandles(): CandleDataInterface[] {
        return this.candles;
    }
}