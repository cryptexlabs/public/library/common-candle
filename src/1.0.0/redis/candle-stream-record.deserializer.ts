import {CandleDataInterface} from "../candle-data.interface";
import {CandleDatumDeserializer} from "../candle-datum.deserializer";
import {CandleSchema} from "../candle-schema";

export class CandleStreamRecordDeserializer {

    private candleDatum: CandleDataInterface[];

    constructor(data: any[]) {
        this.candleDatum = [];
        if (typeof data[0] === "undefined") {
            return;
        }
        if (typeof data[1] === "undefined") {
            return;
        }
        const rawRecordData = data[1];
        if (typeof rawRecordData[0] === "undefined") {
            return;
        }
        if (typeof rawRecordData[1] === "undefined") {
            return;
        }
        if (rawRecordData[0] !== CandleSchema.getCandleUpdateStreamKey()) {
            return;
        }
        const jsonCandleData = rawRecordData[1];
        if (jsonCandleData === "") {
            return;
        }
        const rawCandleDatum = JSON.parse(jsonCandleData);

        const deserializer = new CandleDatumDeserializer(rawCandleDatum);

        this.candleDatum = deserializer.getCandleDatum();
    }

    public getCandleDatum(): CandleDataInterface[] {
        return this.candleDatum;
    }
}